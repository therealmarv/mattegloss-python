from nose.tools import raises
from paintshop import run

def test_doc_example1():
    assert run('5\n1 M 3 G 5 G\n2 G 3 M 4 G\n5 M') == 'G G G G M'

def test_doc_example2():
    assert run('5\n2 M\n5 G\n1 G\n5 G 1 G 4 M\n3 G\n5 G\n3 G 5 G 1 G\n3 G\n2 M\n5 G 1 G\n2 M\n5 G\n4 M\n5 G 4 M') == 'G M G M G'

def test_doc_example3():
    assert run('2\n1 G 2 M\n1 M') == 'M M'

@raises(SystemExit)
def test_doc_example3():
    assert run('4\n1 G 2 G\n2 M\n3 M\n4 M\n1 M') == 'No solution exists'

@raises(SystemExit)
def test_forced_colors_invalid1():
    assert run('1\n1 M\n1 G') == 'No solution exists'

@raises(SystemExit)
def test_forced_colors_invalid2():
    assert run('5\n1 M 3 G 5 G\n2 G 3 M 4 G\n5 M\n5 G') == 'No solution exists'
