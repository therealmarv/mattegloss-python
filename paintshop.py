import sys
import re

def no_solution(exitcode = 1):
    print('No solution exists')
    exit(exitcode)

def check_string_pattern(s, regex):
    pattern = re.compile(regex)
    return pattern.match(s)

def check_regex(lines):
    # check minimum 2 lines
    if (len(lines) < 2):
        no_solution(2)
    # check first line is only a number
    if (not check_string_pattern(lines[0], r'^\d+$')):
        no_solution(2)
    # check if following lines matching regex pattern for colors
    for i in range(1, len(lines)):
        if (not check_string_pattern(lines[i], r'^(\d+ [G|M])( \d+ [G|M])*$')):
            no_solution(2)

def run(input):
    lines = []
    for line in input.split('\n'):
        lines.append(line)
    # check if everything is well formatted
    check_regex(lines)
    # parse customers and number of colors
    number_of_colors = int(lines[0])
    customers = lines[1:]

    # get dictionary representation of all customer colors and forced colors e.g. {1: "M"}
    solution = ['G'] * number_of_colors     # fill solutions with gloss color first
    customer_colors = []
    forced_colors = {}
    for customer in customers:
        colors = {}
        raw_colors = customer.split()
        for i in range(0, len(raw_colors), 2):
            j = int(raw_colors[i])
            c = raw_colors[i+1]
            # input check if color number bigger than available color
            if (j > number_of_colors):
                no_solution(2)
            # input check
            if (c != 'G' and c != 'M'):
                no_solution(2)
            j = j - 1
            # input check if contradicting colors
            if (j in colors and colors[j] != c):
                no_solution(2)
            colors[j] = c
        # input check if there is more than one M
        count_matte = 0
        for c in colors.values():
            if (c == 'M'):
                count_matte = count_matte + 1
        if count_matte > 1:
            no_solution(2)
        customer_colors.append(colors)

    # go through all forced / read-only colors
    for colors in customer_colors:
        if (len(colors) == 1):
            i = list(colors.keys())[0]
            c = colors[i]
            # check if contradicting forced colors
            if (i in forced_colors and forced_colors[i] != c):
                no_solution()
            forced_colors[i] = c
            solution[i] = c

    # now check happyness of all remaining customer preferences
    everybody_happy = False
    while not everybody_happy:
        swapped = 0
        for index, colors in enumerate(customer_colors):
            if (index == 0):
                everybody_happy = True
            if (len(colors) >= 2):
                happy = False
                for i, c in colors.items():
                    if (solution[i] == c):
                        happy = True
                        break
                if (happy):
                    everybody_happy = everybody_happy and happy
                else:
                    everybody_happy = False
                    # swap one color to matte if possible
                    before_swap = swapped
                    for i, c in colors.items():
                        if (c == 'M'):
                            if (i in forced_colors and forced_colors[i] != c):
                                no_solution()
                            else:
                                solution[i] = c
                                swapped = swapped + 1
                                break
                    if (swapped > before_swap):     # if a swap happened recheck from the beginning!
                        break
        if (not everybody_happy and swapped == 0):    # one or more unhappy and no swapping possible
            no_solution()

    return ' '.join(solution)

if __name__ == '__main__':
    cleaned_lines = ''
    for line in sys.stdin:
        cleaned_lines = cleaned_lines + line
    cleaned_lines = cleaned_lines.rstrip('\n')  # remove last newline
    print(run(cleaned_lines))