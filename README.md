# Paintshop problem

This program is written and tested with python3 but should also run with python2.

## Run program

How to run it on a Linux system (no additional packages needed):
```bash
echo -e "5\n1 M 3 G 5 G\n2 G 3 M 4 G\n5 M" | python3 paintshop.py
```

## Run tests

(Optional) create a virtualenv  
Install Nosetests once with `pip3 install -r requirements.txt`

Tests:
```bash
nosetests test_paintshop.py
```

## How it works internally

1) Fill a solution array with glossy color.
2) Fill a dictionary with forced/read only colors. Forced colors are customer colors with only one tuple like e.g. `3 M`. Check for contradictions while doing that.
3) Iterate through all non forced (multiple tuples) customer colors and check if the customers are happy (when at least one color matches their expectations). If a customer is not happy swap a color to matte if possible. When the swap is against a forced color the program will abort (`No solution exists`, exitcode 1).
4) If a swap happened (on a non forced color) go back to step 3 and check if all customers are happy without swapping. When yes, return successful result. If one or more customers are unhappy and no swap is possible the program will abort.

Note: The program also checks the validity of the input (color numbers, format) and stops on failed input validation with exitcode 2, `No solution exists`.

---

Marvin Reimer